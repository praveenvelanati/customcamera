//
//  ViewController.swift
//  CustomCamera
//
//  Created by praveen velanati on 3/24/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

  var camera = true
    
    @IBOutlet weak var previewView: UIView!
    
    @IBOutlet weak var capturedImage: UIImageView!
    
    var captureSession : AVCaptureSession?
    var stillImageOutput : AVCaptureStillImageOutput?
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    
   // var defaultCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    
    override func viewDidLoad() {
        super.viewDidLoad()
       }

 
    
   @IBAction func switchCamera(sender: UIButton) {
        
        camera = !camera
        reloadCamera()
        
           }
    
    
    
    func reloadCamera() {
        
        
        captureSession = AVCaptureSession()
        
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        var defaultCamera : AVCaptureDevice! 
        
        if (camera == false) {
            
         defaultCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
       
        }  else {
            
            let Devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
            
            for device in Devices {
                
                let device = device as! AVCaptureDevice
                
                if (device.position == AVCaptureDevicePosition.Front) {
                    
                    
                    defaultCamera = device
                }
                
            }
            
            }
    
        do {
            
            try  defaultCamera.lockForConfiguration()
        } catch let err as NSError {
            
            print(err.localizedDescription)
        }
        
        
        if (defaultCamera.isFlashModeSupported(AVCaptureFlashMode.Auto))
        { defaultCamera.flashMode = AVCaptureFlashMode.Auto
        }
        
        if (defaultCamera.isFocusModeSupported(AVCaptureFocusMode.AutoFocus)){
        defaultCamera.focusMode = AVCaptureFocusMode.AutoFocus
        }
        
         defaultCamera.unlockForConfiguration()
        
        var error : NSError?
        
        var input : AVCaptureDeviceInput!
        
        do {   input = try AVCaptureDeviceInput(device: defaultCamera)
            
        } catch let err as NSError {
            
            print(err.localizedDescription)
        }
        
        if error == nil && captureSession!.canAddInput(input) {
            
            captureSession!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
            
            if captureSession!.canAddOutput(stillImageOutput) {
                
                captureSession!.addOutput(stillImageOutput)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                previewView.layer.addSublayer(previewLayer!)
                
                captureSession!.startRunning()
                
            }
            
        }
        

        
        
        
        
        
    }
    
    
    
    
    
    
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
      
        reloadCamera()
        
        
    }
    
    
    override func viewDidAppear(animated: Bool) {
    
        super.viewDidAppear(animated)
        
        previewLayer!.frame = previewView.bounds
        
        
    }
    
    
    
    
    @IBAction func didPressTakePhoto(sender: UIButton) {
        
        
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {
            
            videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: { (sampleBuffer, error) -> Void in
                
                if (sampleBuffer != nil) {
                    
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    
                    self.capturedImage.image = image
                    
                }
                
            })
            }
    
           }

}

